package com.optal.pages;

import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage extends BasePage {

    @FindBy(id = "menuMyApps")
    private WebElement menuMyApps = null;
    @FindBy(css ="h2.bodyBackgroundHeaderMargin.floatLeft")
    private WebElement welcomeElement = null;
    @FindBy(id = "submenuMyApps")
    private WebElement submenuMyApps = null;


    public LandingPage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean validateLogin(String message) {
        WebWaits.waitForTextToBePresent(welcomeElement, message);
        return welcomeElement.getText().toLowerCase().contains(message.toLowerCase());
    }


    public RequestNewCardPage requestSingleCard() { //cardProfile
        ClickControl.click(menuMyApps);
        Actions action = new Actions(webDriver);
        action.moveToElement(submenuMyApps).moveToElement(webDriver.findElement(By.id("requestCards"))).click().build().perform();
        return PageFactory.initElements(webDriver, RequestNewCardPage.class);
    }
}
