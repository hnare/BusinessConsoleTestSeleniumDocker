import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"C:/WIP/docker/BusinessConsoleTestSeleniumDocker/BusinesConsoleUiTest/src/test/resources/Features/UC001Login.feature:8"},
        plugin = {"pretty"},
        monochrome = true,
        glue = {"com.optal"})
public class Parallel01IT {
}
